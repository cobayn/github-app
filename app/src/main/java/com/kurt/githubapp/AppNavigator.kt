package com.kurt.githubapp

interface AppNavigator {
    fun openRepoDetails(repo: String, owner: String)
    fun openUserDetails(username: String)
}