package com.kurt.githubapp

import android.app.Activity
import android.app.Application
import com.kurt.githubapp.di.DaggerApplicationComponent
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject

class GithubApp : Application(), HasActivityInjector {

    @Inject
    lateinit var dispatchAndroidInjector: DispatchingAndroidInjector<Activity>

    override fun onCreate() {
        super.onCreate()
        loadDagger()
    }

    private fun loadDagger() {
        DaggerApplicationComponent.factory().create(this).inject(this)
    }

    override fun activityInjector(): AndroidInjector<Activity> {
        return dispatchAndroidInjector
    }

}