package com.kurt.githubapp.utiil

interface ExtendedController {

    fun openExternalLink(link: String)
    fun openUserDetails(username: String)
    fun openRepoDetails(owner: String, repo: String)

}