package com.kurt.githubapp.exception

import android.content.Context
import com.kurt.domain.exception.NoInternetException
import com.kurt.domain.exception.NoRepositoryDetailException
import com.kurt.domain.exception.NoUserDetailException
import com.kurt.githubapp.R
import javax.inject.Inject

/**
 * check instance of exception and show error message to user
 */

class ErrorMessageFactory @Inject constructor(private val context: Context) {

    private val defaultMessage = context.getString(R.string.default_error_message)

    fun getString(e: Exception) : String{
        return when (e){
            is NoUserDetailException -> context.getString(R.string.user_error_message)
            is NoRepositoryDetailException -> context.getString(R.string.repo_error_message)
            is NoInternetException -> context.getString(R.string.internet_error_message)
            else -> defaultMessage
        }
    }

}