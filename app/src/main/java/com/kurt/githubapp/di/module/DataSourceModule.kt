package com.kurt.githubapp.di.module

import com.kurt.data.repositories.repoDetailRepository.RepoDetailDataSource
import com.kurt.data.repositories.repoDetailRepository.RepoDetailDataSourceImpl
import com.kurt.data.repositories.repoRepository.RepoDataSource
import com.kurt.data.repositories.repoRepository.RepoDataSourceImpl
import com.kurt.data.repositories.userRepository.UserDetailDataSource
import com.kurt.data.repositories.userRepository.UserDetailDataSourceImpl
import dagger.Binds
import dagger.Module

@Module
abstract class DataSourceModule {

    @Binds
    abstract fun provideRepoDataSource(repoDataSourceImpl: RepoDataSourceImpl): RepoDataSource

    @Binds
    abstract fun provideRepoDetailDataSource(repoDetailDataSourceImpl: RepoDetailDataSourceImpl): RepoDetailDataSource

    @Binds
    abstract fun provideUserDetailDataSource(userDetailDataSourceImpl: UserDetailDataSourceImpl): UserDetailDataSource

}