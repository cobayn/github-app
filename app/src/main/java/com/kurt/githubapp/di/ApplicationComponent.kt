package com.kurt.githubapp.di

import com.kurt.data.di.NetworkModule
import com.kurt.githubapp.GithubApp
import com.kurt.githubapp.di.module.ActtivityBuilderModule
import com.kurt.githubapp.di.module.AppModule
import com.kurt.githubapp.di.module.DataSourceModule
import com.kurt.githubapp.di.module.RepositoryModule
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AndroidSupportInjectionModule::class,
    ActtivityBuilderModule::class,
    AppModule::class,
    NetworkModule::class,
    DataSourceModule::class,
    RepositoryModule::class
])
interface ApplicationComponent : AndroidInjector<GithubApp> {

    @Component.Factory
    abstract class Factory : AndroidInjector.Factory<GithubApp>

}