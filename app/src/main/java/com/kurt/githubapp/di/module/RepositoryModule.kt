package com.kurt.githubapp.di.module

import com.kurt.data.repositories.repoDetailRepository.RepoDetailRepositoryImpl
import com.kurt.data.repositories.repoRepository.RepoRepositoryImpl
import com.kurt.data.repositories.userRepository.UserDetailRepositoryImpl
import com.kurt.domain.reposiitory.RepoDetailsRepository
import com.kurt.domain.reposiitory.SearchRepository
import com.kurt.domain.reposiitory.UserDetailsRepository
import dagger.Binds
import dagger.Module

@Module
abstract class RepositoryModule {

    @Binds
    abstract fun provideRepoRepository(repositoryImpl: RepoRepositoryImpl): SearchRepository

    @Binds
    abstract fun provideRepoDetailsRepository(repoDetailRepositoryImpl: RepoDetailRepositoryImpl): RepoDetailsRepository

    @Binds
    abstract fun provideUserDetailsRepository(userDetailsRepositoryImpl: UserDetailRepositoryImpl): UserDetailsRepository

}