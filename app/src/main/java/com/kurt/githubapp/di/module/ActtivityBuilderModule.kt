package com.kurt.githubapp.di.module

import com.kurt.githubapp.view.activity.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActtivityBuilderModule {

    @ContributesAndroidInjector(modules = [FragmentBinder::class])
    abstract fun mainActivity(): MainActivity

}