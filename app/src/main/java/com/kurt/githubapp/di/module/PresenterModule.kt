package com.kurt.githubapp.di.module

import com.kurt.githubapp.presenter.*
import com.kurt.githubapp.view.RepoDetailsView
import com.kurt.githubapp.view.RepoView
import com.kurt.githubapp.view.UserDetailsView
import com.kurt.githubapp.view.fragment.RepositoryDetailsFragment
import com.kurt.githubapp.view.fragment.RepositoryFragment
import com.kurt.githubapp.view.fragment.UserDetailsFragment
import dagger.Binds
import dagger.Module

@Module
abstract class PresenterModule {

    @Binds
    abstract fun repoView(repositoryFragment: RepositoryFragment): RepoView

    @Binds
    abstract fun repoPresenter(repositoryPresenterImpl: RepositoryPresenterImpl): RepositoryPresenter

    @Binds
    abstract fun repoDetailsView(repositoryDetailsFragment: RepositoryDetailsFragment): RepoDetailsView

    @Binds
    abstract fun repoDetailPresenter(repoDetailsPresenterImpl: RepoDetailsPresenterImpl): RepoDetailsPresenter

    @Binds
    abstract fun userDetailsView(userDetailsFragment: UserDetailsFragment): UserDetailsView

    @Binds
    abstract fun userDetailsPresenter(userDetailsPresenterImpl: UserDetailsPresenterImpl): UserDetailsPresenter

}