package com.kurt.githubapp.di.module

import android.content.Context
import com.kurt.githubapp.GithubApp
import dagger.Module
import dagger.Provides

@Module
class AppModule {

    @Provides
    fun provideContext(application: GithubApp): Context = application.applicationContext

}