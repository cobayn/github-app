package com.kurt.githubapp.di.module

import com.kurt.githubapp.view.fragment.RepositoryDetailsFragment
import com.kurt.githubapp.view.fragment.RepositoryFragment
import com.kurt.githubapp.view.fragment.UserDetailsFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBinder {

    @ContributesAndroidInjector(modules = [PresenterModule::class])
    abstract fun repositoryFragment(): RepositoryFragment

    @ContributesAndroidInjector(modules = [PresenterModule::class])
    abstract fun repoDetailsFragment(): RepositoryDetailsFragment

    @ContributesAndroidInjector(modules = [PresenterModule::class])
    abstract fun userDetailsFragment(): UserDetailsFragment

}