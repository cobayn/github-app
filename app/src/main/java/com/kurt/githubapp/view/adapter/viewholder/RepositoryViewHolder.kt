package com.kurt.githubapp.view.adapter.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.kurt.domain.Repo
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_repository.view.*

class RepositoryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bind(repo: Repo) {
        itemView.tvRepoName.text = repo.name
        itemView.tvAuthorName.text = repo.owner.login
        itemView.tvWatchersNumber.text = repo.watchers.toString()
        itemView.tvForkNumber.text = repo.forks.toString()
        itemView.tvIssueNumber.text = repo.open_issues_count.toString()
        Picasso.get().load(repo.owner.avatar_url).into(itemView.ivPoster)
    }

}