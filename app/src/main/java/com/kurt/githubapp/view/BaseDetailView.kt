package com.kurt.githubapp.view

import com.kurt.domain.DetailModel

interface BaseDetailView<T : DetailModel> : BaseView {

    fun setDetails(detailModel: T)

}