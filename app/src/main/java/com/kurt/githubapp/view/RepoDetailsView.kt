package com.kurt.githubapp.view

import com.kurt.domain.RepoDetails

interface RepoDetailsView : BaseDetailView<RepoDetails>{}