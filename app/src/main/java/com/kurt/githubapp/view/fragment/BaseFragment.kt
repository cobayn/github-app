package com.kurt.githubapp.view.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.kurt.githubapp.extension.toast
import com.kurt.githubapp.presenter.BasePresenter
import com.kurt.githubapp.utiil.ExtendedController
import com.kurt.githubapp.view.BaseView
import dagger.android.support.DaggerFragment
import javax.inject.Inject

abstract class BaseFragment<V : BaseView, P : BasePresenter> : DaggerFragment(), BaseView {

    @Inject
    lateinit var presenter: P

    protected var extendedController: ExtendedController? = null

    protected abstract fun getLayout(): Int

    protected abstract fun getProgressBar(): View

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(getLayout(), container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is ExtendedController)
            extendedController = context
    }

    override fun onDestroy() {
        extendedController = null
        presenter.detachView()
        presenter.destroyJob()
        super.onDestroy()
    }

    override fun errorMessage(message: String) {
        toast(message, Toast.LENGTH_SHORT)
    }

    override fun hideLoading() {
        getProgressBar().apply {
            visibility = View.GONE
        }
    }

    override fun showLoading() {
        getProgressBar().apply {
            visibility = View.VISIBLE
        }
    }

}