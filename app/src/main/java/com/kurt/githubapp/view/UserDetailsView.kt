package com.kurt.githubapp.view

import com.kurt.domain.User
import com.kurt.domain.UserDetails

interface UserDetailsView : BaseDetailView<UserDetails>{}