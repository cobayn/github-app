package com.kurt.githubapp.view.adapter

import androidx.recyclerview.widget.DiffUtil
import com.kurt.domain.Repo

class RepositoryDiiffUtil(
    private val oldRepo: List<Repo>,
    private val newRepo: List<Repo>
) : DiffUtil.Callback() {
    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldRepo[oldItemPosition].id == newRepo[newItemPosition].id
    }

    override fun getOldListSize() = oldRepo.size

    override fun getNewListSize() = newRepo.size

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldRepo[oldItemPosition].equals(newRepo[newItemPosition])
    }

}