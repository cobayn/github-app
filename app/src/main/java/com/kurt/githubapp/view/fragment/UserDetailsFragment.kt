package com.kurt.githubapp.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.kurt.domain.User
import com.kurt.domain.UserDetails
import com.kurt.githubapp.R
import com.kurt.githubapp.extension.gone
import com.kurt.githubapp.extension.show
import com.kurt.githubapp.presenter.UserDetailsPresenter
import com.kurt.githubapp.view.BaseDetailView
import com.kurt.githubapp.view.UserDetailsView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_user_detail.*
import javax.inject.Inject

class UserDetailsFragment : BaseDetailFragment<UserDetailsView, UserDetailsPresenter>(), UserDetailsView {

    override fun getLayout() = R.layout.fragment_user_detail

    override fun getProgressBar() = pbUserDetails

    override fun getButton() = btnOpenWebScreen

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.getUserDetails(requireArguments().getString("username"))
    }

    override fun setDetails(detailModel: UserDetails) {
        getButtonView(detailModel)
        Picasso.get().load(detailModel.avatar_url).into(ivAvatarPoster)
        tvNameValue.text = checkNotNullInfo(detailModel.name)
        tvCompanyValue.text = checkNotNullInfo(detailModel.company)
        tvEmailValue.text = checkNotNullInfo(detailModel.email)
        tvLocationValue.text = checkNotNullInfo(detailModel.location)
    }

    override fun showLoading() {
        super.showLoading()
        btnOpenWebScreen.gone()
        cvUserInfo.gone()
        ivAvatarPoster.gone()
    }

    override fun hideLoading() {
        super.hideLoading()
        btnOpenWebScreen.show()
        cvUserInfo.show()
        ivAvatarPoster.show()
    }

}