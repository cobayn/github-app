package com.kurt.githubapp.view.fragment

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import androidx.appcompat.widget.SearchView
import androidx.core.os.bundleOf
import androidx.core.view.get
import androidx.navigation.Navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.kurt.domain.Repo
import com.kurt.githubapp.AppNavigator
import com.kurt.githubapp.R
import com.kurt.githubapp.extension.hideSoftInput
import com.kurt.githubapp.presenter.RepositoryPresenter
import com.kurt.githubapp.view.RepoView
import com.kurt.githubapp.view.adapter.RepositoryAdapter
import kotlinx.android.synthetic.main.fragment_repository.*
import javax.inject.Inject

class RepositoryFragment : BaseFragment<RepoView, RepositoryPresenter>(), RepoView, AppNavigator, AdapterView.OnItemSelectedListener {

    private val repoAdapter = RepositoryAdapter(this)

    override fun getLayout() = R.layout.fragment_repository

    override fun getProgressBar() = pbSearch

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rvSearch.apply {

            layoutManager = LinearLayoutManager(requireContext())
            adapter = repoAdapter

        }

        svSearchRepository.apply {
            setOnQueryTextListener(object : SearchView.OnQueryTextListener {

                override fun onQueryTextChange(newText: String?): Boolean {
                    presenter.searchRepository(newText, spnSort.selectedItem.toString())
                    return true
                }

                override fun onQueryTextSubmit(query: String?): Boolean {
                    hideSoftInput()
                    presenter.searchRepository(query, spnSort.selectedItem.toString())
                    return true
                }

            })
        }

        spnSort.onItemSelectedListener = this
    }

    override fun setData(data: List<Repo>) {
        repoAdapter.repoItems = data
    }

    override fun openRepoDetails(repo: String, owner: String) {
        extendedController?.openRepoDetails(owner, repo)
    }

    override fun openUserDetails(username: String) {
        extendedController?.openUserDetails(username)
    }

    override fun onItemSelected(adapterView: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
        presenter.searchRepository(svSearchRepository.query.toString(), adapterView?.selectedItem.toString())
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {

    }

}