package com.kurt.githubapp.view.fragment

import android.view.View
import com.kurt.domain.DetailModel
import com.kurt.githubapp.presenter.BasePresenter
import com.kurt.githubapp.view.BaseView

abstract class BaseDetailFragment<V: BaseView, P : BasePresenter> : BaseFragment<V, P>() {

    /**
     * get button in repodetails or userdetails fragment to set click listener for show
     * infos in browser
     */
    protected abstract fun getButton(): View

    protected fun getButtonView(detailModel: DetailModel) = with(getButton()){
        setOnClickListener {
            extendedController?.openExternalLink(detailModel.html_url)
        }
    }

    /**
     * check which detail information is null
     * if is null, place -
     */
    protected fun checkNotNullInfo(info: String?): String{
        return if (info.isNullOrEmpty()) "-" else info
    }

}