package com.kurt.githubapp.view.activity

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.kurt.githubapp.R
import com.kurt.githubapp.utiil.ExtendedController
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.DaggerAppCompatActivity
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

class MainActivity : DaggerAppCompatActivity(), HasSupportFragmentInjector, ExtendedController {

    @Inject
    lateinit var supportDispatcherFragmentInjector: DispatchingAndroidInjector<Fragment>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
        return supportDispatcherFragmentInjector
    }

    /**
     * open repo or detail information in external browser
     */
    override fun openExternalLink(link: String) {
        val broserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(link))
        startActivity(broserIntent)
    }

    /**
     * show repository details
     */
    override fun openRepoDetails(owner: String, repo: String) {
        Navigation.findNavController(this, R.id.nav_fragment)
            .navigate(R.id.repositoryDetailsFragment, bundleOf("repo" to repo, "owner" to owner))
    }

    /**
     * show user details
     */
    override fun openUserDetails(username: String) {
        Navigation.findNavController(this, R.id.nav_fragment)
            .navigate(R.id.userDetailsFragment, bundleOf("username" to username))
    }

}
