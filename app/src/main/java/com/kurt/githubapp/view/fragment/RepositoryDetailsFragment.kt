package com.kurt.githubapp.view.fragment

import android.os.Bundle
import android.view.View
import com.kurt.domain.DetailModel
import com.kurt.domain.Repo
import com.kurt.domain.RepoDetails
import com.kurt.githubapp.R
import com.kurt.githubapp.extension.gone
import com.kurt.githubapp.extension.show
import com.kurt.githubapp.presenter.RepoDetailsPresenter
import com.kurt.githubapp.view.BaseDetailView
import com.kurt.githubapp.view.RepoDetailsView
import kotlinx.android.synthetic.main.fragment_repository_detail.*
import javax.inject.Inject

class RepositoryDetailsFragment : BaseDetailFragment<RepoDetailsView, RepoDetailsPresenter>(), RepoDetailsView {

    override fun getLayout() = R.layout.fragment_repository_detail

    override fun getProgressBar() = pbRepoDetails

    override fun getButton() = btnOpenWebRepo

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.getRepoDetails(
            requireArguments().getString("owner"),
            requireArguments().getString("repo")
        )

        btnOpenUserScreen.setOnClickListener {
            extendedController?.openUserDetails(requireArguments().getString("owner", ""))
        }

    }

    override fun setDetails(detailModel: RepoDetails) {
        getButtonView(detailModel)
        tvProgramingLanguageValue.text = checkNotNullInfo(detailModel.language)
        tvDateCreateValue.text = checkNotNullInfo(detailModel.created_at)
        tvDateOfChangeValue.text = checkNotNullInfo(detailModel.updated_at)
    }

    override fun showLoading() {
        super.showLoading()
        cvRepoInfo.gone()
        btnOpenUserScreen.gone()
        btnOpenWebRepo.gone()
    }

    override fun hideLoading() {
        super.hideLoading()
        cvRepoInfo.show()
        btnOpenUserScreen.show()
        btnOpenWebRepo.show()
    }

}