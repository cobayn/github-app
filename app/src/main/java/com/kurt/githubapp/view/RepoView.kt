package com.kurt.githubapp.view

import com.kurt.domain.Repo

interface RepoView : BaseView {

    fun setData(data: List<Repo>)

}