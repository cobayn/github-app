package com.kurt.githubapp.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.kurt.domain.Repo
import com.kurt.githubapp.AppNavigator
import com.kurt.githubapp.R
import com.kurt.githubapp.view.adapter.viewholder.RepositoryViewHolder
import kotlinx.android.synthetic.main.item_repository.view.*
import kotlin.properties.Delegates


class RepositoryAdapter(private val appNavigator: AppNavigator) : RecyclerView.Adapter<RepositoryViewHolder>() {

    internal var repoItems: List<Repo> by Delegates.observable(emptyList()){property, oldValue, newValue ->
        val diffResult = DiffUtil.calculateDiff(RepositoryDiiffUtil(oldValue, newValue))
        diffResult.dispatchUpdatesTo(this)
    }

    override fun getItemCount(): Int {
        return repoItems.size
    }

    override fun onBindViewHolder(holder: RepositoryViewHolder, position: Int) {
        holder.bind(repoItems[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RepositoryViewHolder {
        val viewHolder = RepositoryViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_repository, parent, false))
        viewHolder.itemView.setOnClickListener { appNavigator.openRepoDetails(repoItems[viewHolder.adapterPosition].name, repoItems[viewHolder.adapterPosition].owner.login) }
        viewHolder.itemView.ivPoster.setOnClickListener { appNavigator.openUserDetails(repoItems[viewHolder.adapterPosition].owner.login) }
        return viewHolder
    }

}