package com.kurt.githubapp.view

interface BaseView {

    fun showLoading()
    fun hideLoading()
    fun errorMessage(message: String)

}