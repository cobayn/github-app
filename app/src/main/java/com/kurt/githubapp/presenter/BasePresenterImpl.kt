package com.kurt.githubapp.presenter

import com.kurt.domain.exception.NoInternetException
import com.kurt.githubapp.exception.ErrorMessageFactory
import com.kurt.githubapp.view.BaseView
import kotlinx.coroutines.*

abstract class BasePresenterImpl<V : BaseView> constructor(
    protected var mView: V?,
    private val errorMessageFactory: ErrorMessageFactory
) : BasePresenter {

    protected var job: Job? = null

    protected val coroutineExceptionHandler = CoroutineExceptionHandler{_, _ ->
        mView?.hideLoading()
        mView?.errorMessage(errorMessageFactory.getString(NoInternetException()))
    }

    override fun detachView() {
        mView = null
    }

    override fun destroyJob() {
        job?.cancel()
    }

    protected suspend fun handleErrorMessage(e: Exception) {
        withContext(Dispatchers.Main) {
            mView?.hideLoading()
            mView?.errorMessage(errorMessageFactory.getString(e))
        }
    }

}