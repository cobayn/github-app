package com.kurt.githubapp.presenter

interface BasePresenter {
    fun detachView()
    fun destroyJob()
}