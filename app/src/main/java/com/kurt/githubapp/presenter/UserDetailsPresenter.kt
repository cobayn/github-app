package com.kurt.githubapp.presenter

interface UserDetailsPresenter : BasePresenter {

    fun getUserDetails(username: String?)

}