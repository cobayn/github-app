package com.kurt.githubapp.presenter

import com.kurt.domain.UserDetailParam
import com.kurt.domain.interactor.UserDetailsUsecase
import com.kurt.githubapp.exception.ErrorMessageFactory
import com.kurt.githubapp.view.UserDetailsView
import kotlinx.coroutines.*
import javax.inject.Inject

class UserDetailsPresenterImpl @Inject constructor(
    mView: UserDetailsView,
    errorMessageFactory: ErrorMessageFactory,
    private val userDetailsUsecase: UserDetailsUsecase
): BasePresenterImpl<UserDetailsView>(mView, errorMessageFactory), UserDetailsPresenter {

    override fun getUserDetails(username: String?) {

        if (username != null) {
            mView?.showLoading()
            job = CoroutineScope(userDetailsUsecase.dispatcher).launch {
                try {
                    val userDetails = userDetailsUsecase.invoke(UserDetailParam(username))
                        if (userDetails != null) {
                            mView?.setDetails(userDetails)
                        }
                        mView?.hideLoading()
                } catch (e: Exception){
                    handleErrorMessage(e)
                }
            }
        }
    }

}