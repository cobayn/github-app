package com.kurt.githubapp.presenter

import com.kurt.domain.interactor.SearchUsecase
import com.kurt.domain.interactor.launchObserve
import com.kurt.githubapp.exception.ErrorMessageFactory
import com.kurt.githubapp.view.RepoView
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.channels.sendBlocking
import kotlinx.coroutines.flow.*
import java.lang.Exception
import javax.inject.Inject

class RepositoryPresenterImpl @Inject constructor(
    mView: RepoView,
    errorMessageFactory: ErrorMessageFactory,
    private val searchUsecase: SearchUsecase
) : BasePresenterImpl<RepoView>(mView, errorMessageFactory), RepositoryPresenter {

    private val searchQuery = ConflatedBroadcastChannel<SearchUsecase.Params>()

    init {

        CoroutineScope(searchUsecase.dispatcher).launch(searchUsecase.dispatcher + coroutineExceptionHandler) {
            searchQuery.asFlow()
                .debounce(600)
                .collectLatest {
                        val newJob = async(searchUsecase.dispatcher) {
                            searchUsecase.invoke(it)
                        }
                        newJob.await()

                }
        }

        CoroutineScope(searchUsecase.dispatcher).launchObserve(searchUsecase, coroutineExceptionHandler) {
            it.collect {
                        mView.hideLoading()
                        mView.setData(it)
            }
        }

    }

    override fun searchRepository(query: String?, sort: String) {
        if (query != null) {
            mView?.showLoading()
            searchQuery.sendBlocking(SearchUsecase.Params(query, sort))
        }
    }

}