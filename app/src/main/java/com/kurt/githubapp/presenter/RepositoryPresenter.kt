package com.kurt.githubapp.presenter

interface RepositoryPresenter : BasePresenter {

    fun searchRepository(query: String?, sort: String)

}