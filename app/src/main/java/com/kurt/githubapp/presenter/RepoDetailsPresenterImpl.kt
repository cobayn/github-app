package com.kurt.githubapp.presenter

import com.kurt.domain.RepoDetaiilParam
import com.kurt.domain.interactor.RepoDetailsUsecase
import com.kurt.githubapp.exception.ErrorMessageFactory
import com.kurt.githubapp.view.RepoDetailsView
import kotlinx.coroutines.*
import javax.inject.Inject

class RepoDetailsPresenterImpl @Inject constructor(
    mView: RepoDetailsView,
    errorMessageFactory: ErrorMessageFactory,
    private val repoDetailsUsecase: RepoDetailsUsecase
): BasePresenterImpl<RepoDetailsView>(mView, errorMessageFactory), RepoDetailsPresenter {

    override fun getRepoDetails(owner: String?, repo: String?) {
        if (owner != null && repo != null) {
            mView?.showLoading()
            job = CoroutineScope(repoDetailsUsecase.dispatcher).launch {
                try {
                    val repoDetails = repoDetailsUsecase.invoke(RepoDetaiilParam(repo, owner))
                        if (repoDetails != null) {
                            mView?.setDetails(repoDetails)
                        }
                        mView?.hideLoading()
                } catch (e: Exception){
                    handleErrorMessage(e)
                }
            }
        }
    }

}