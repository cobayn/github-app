package com.kurt.githubapp.presenter

interface RepoDetailsPresenter : BasePresenter {

    fun getRepoDetails(owner: String?, repo: String?)

}