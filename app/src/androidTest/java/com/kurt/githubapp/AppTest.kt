package com.kurt.githubapp

import android.content.Intent
import android.os.SystemClock
import android.view.View
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.*
import androidx.test.espresso.UiController
import androidx.test.espresso.ViewAction
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import com.kurt.githubapp.view.activity.MainActivity
import org.hamcrest.Matcher
import org.hamcrest.Matchers.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class AppTest {

    @Rule
    @JvmField
    val activityRUle = ActivityTestRule<MainActivity>(MainActivity::class.java, true, false)

    @Before
    fun setUp() {
        activityRUle.launchActivity(Intent())
    }

    @Test
    fun testApp() {
        onView(withId(R.id.svSearchRepository)).perform(click())
        SystemClock.sleep(1000)
        onView(withId(R.id.svSearchRepository)).perform(typeSearchViewText("tr"))
        SystemClock.sleep(4000)
        onView(withId(R.id.spnSort)).perform(click())
        onData(allOf(`is`(instanceOf(String::class.java)), `is`("stars"))).perform(click())
        SystemClock.sleep(3000)
        onView(withId(R.id.rvSearch)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(1, clickOnViewChild(R.id.ivPoster)))
        SystemClock.sleep(2500)
        pressBack()
        onView(withId(R.id.rvSearch)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(1, click()))
        SystemClock.sleep(2000)

    }

    fun typeSearchViewText(text: String?): ViewAction? {
        return object : ViewAction {
            override fun getConstraints(): Matcher<View> { //Ensure that only apply if it is a SearchView and if it is visible.
                return allOf(isDisplayed(), isAssignableFrom(SearchView::class.java))
            }

            override fun getDescription(): String {
                return "Change view text"
            }

            override fun perform(uiController: UiController?, view: View) {
                (view as SearchView).setQuery(text, false)
            }
        }
    }

    fun clickOnViewChild(viewId: Int) = object : ViewAction {
        override fun getConstraints() = null

        override fun getDescription() = "Click on a child view with specified id."

        override fun perform(uiController: UiController, view: View) = click().perform(uiController, view.findViewById<View>(viewId))
    }

}