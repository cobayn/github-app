package com.kurt.data.repositories.userRepository

import com.kurt.data.entity.ErrorResult
import com.kurt.data.entity.Success
import com.kurt.data.entity.UserDetailsEntiity
import com.kurt.data.mapper.UserDetailsEntityToUserDetails
import com.kurt.domain.reposiitory.UserDetailsRepository
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.instanceOf
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class UserRepositoryTest {

    private val FAKE_MESSAGE = "PAO USER"
    private val FAKE_USERNAME = "makurt"
    private val FAKE_ID = 100L

    @Mock
    lateinit var userDataSource: UserDetailDataSource
    @Mock
    lateinit var userDetailsMapper: UserDetailsEntityToUserDetails

    private lateinit var userRepository: UserDetailsRepository

    @Before
    fun setUp() {
        userRepository = UserDetailRepositoryImpl(userDataSource, userDetailsMapper)
    }

    @Test
    fun testErrorUserData() = runBlocking {
        given(userDataSource.getUserDetails(FAKE_USERNAME)).willReturn(ErrorResult(Throwable(FAKE_MESSAGE), FAKE_MESSAGE))
        val errorData = userDataSource.getUserDetails(FAKE_USERNAME)
        assertThat(errorData, `is`(instanceOf(ErrorResult::class.java)))
        assertThat((errorData as ErrorResult).message, `is`(FAKE_MESSAGE))
        assertThat(errorData.throwable?.message, `is`(FAKE_MESSAGE))
    }

    @Test
    fun testSuccessUserData() = runBlocking {
        given(userDataSource.getUserDetails(FAKE_USERNAME)).willReturn(Success(createUserEntity()))
        val data = userDataSource.getUserDetails(FAKE_USERNAME)
        assertThat(data, `is`(instanceOf(Success::class.java)))
        assertThat((data as Success).data, `is`(instanceOf(UserDetailsEntiity::class.java)))
        assertThat(data.data.id, `is`(FAKE_ID))
    }

    private fun createUserEntity(): UserDetailsEntiity {
        return UserDetailsEntiity(
            id = FAKE_ID,
            html_url = "html_url",
            name = "cobayn",
            login = "makurt",
            avatar_url = "avatar",
            bio = "bio",
            email = "email",
            company = "undabot",
            location = "Zagreb"
        )
    }

}