package com.kurt.data.repositories.repoDetailsRepository

import com.kurt.data.entity.ErrorResult
import com.kurt.data.entity.RepositoryDetaiilsEntiity
import com.kurt.data.entity.Success
import com.kurt.data.entity.UserEntity
import com.kurt.data.mapper.RepositoryDetailsEntityToRepositoryDetails
import com.kurt.data.repositories.repoDetailRepository.RepoDetailDataSource
import com.kurt.data.repositories.repoDetailRepository.RepoDetailRepositoryImpl
import com.kurt.domain.reposiitory.RepoDetailsRepository
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.instanceOf
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers
import org.mockito.BDDMockito.given
import org.mockito.BDDMockito.notNull
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class RepoDetailsRepositoryTest {

    private val FAKE_MESSAGE = "PAO REPO"
    private val FAKE_REPO = "tetris"
    private val FAKE_USER = "makurt"
    private val FAKE_ID = 100L

    @Mock
    lateinit var repoDetailDataSource: RepoDetailDataSource
    @Mock
    lateinit var repoMapper: RepositoryDetailsEntityToRepositoryDetails

    private lateinit var repoDetailRepository: RepoDetailsRepository

    @Before
    fun setUp() {
        repoDetailRepository = RepoDetailRepositoryImpl(repoDetailDataSource, repoMapper)
    }

    @Test
    fun testErrorRepoDetailsData() = runBlocking {
        given(repoDetailDataSource.getRepoDetails(FAKE_USER, FAKE_REPO)).willReturn(ErrorResult(Throwable(FAKE_MESSAGE), FAKE_MESSAGE))
        val errorData = repoDetailDataSource.getRepoDetails(FAKE_USER, FAKE_REPO)
        assertThat(errorData, `is`(instanceOf(ErrorResult::class.java)))
        assertThat((errorData as ErrorResult).message, `is`(FAKE_MESSAGE))
        assertThat(errorData.throwable?.message, `is`(FAKE_MESSAGE))
    }

    @Test
    fun testSuccessRepoDetailsData() = runBlocking {
        given(repoDetailDataSource.getRepoDetails(FAKE_USER, FAKE_REPO)).willReturn(Success(createFakeRepositoryDetail()))
        val repoDetails = repoDetailDataSource.getRepoDetails(FAKE_USER, FAKE_REPO)
        assertThat(repoDetails, `is`(instanceOf(Success::class.java)))
        assertThat((repoDetails as Success).data, `is`(instanceOf(RepositoryDetaiilsEntiity::class.java)))
        assertThat(repoDetails.data.id, `is`(FAKE_ID))
    }

    private fun createFakeRepositoryDetail(): RepositoryDetaiilsEntiity {
        return RepositoryDetaiilsEntiity(
            id = FAKE_ID,
            watchers = 5,
            open_issues_count = 4,
            full_name = "Marko Kurt",
            forks = 2,
            description = "Programer",
            name = "makurt",
            language = "kotlin",
            updated_at = "23.02.2020",
            created_at = "20.02.2020",
            html_url = "html",
            owner = createFakeUserEntity()
        )
    }

    private fun createFakeUserEntity(): UserEntity {
        return UserEntity(
            id = 200,
            avatar_url = "avatar_url",
            login = "makurt"
        )
    }

}