package com.kurt.data.repositories.repoRepository

import com.kurt.data.entity.ErrorResult
import com.kurt.data.entity.RepositoryEntity
import com.kurt.data.entity.Success
import com.kurt.data.entity.UserEntity
import com.kurt.data.mapper.RepositoryEntityToRepo
import com.kurt.domain.reposiitory.SearchRepository
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.instanceOf
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class RepoRepositoryTest {

    private val FAKE_MESSAGE = "PAO SAM"
    private val FAKE_SEARCH = "tetris"
    private val FAKE_SORT = "stars"

    @Mock
    lateinit var repoDataSource: RepoDataSource
    @Mock
    lateinit var repoMapper: RepositoryEntityToRepo
    @Mock
    lateinit var searchStore: SearchStore

    private lateinit var repoRepository: SearchRepository

    @Before
    fun setUp() {
        repoRepository = RepoRepositoryImpl(repoDataSource, repoMapper, searchStore)
    }

    @Test
    fun testErrorSearchData() = runBlocking {
        given(repoDataSource.searchRepos(FAKE_SEARCH, FAKE_SORT)).willReturn(ErrorResult(Throwable(FAKE_MESSAGE), FAKE_MESSAGE))
        val errorData = repoDataSource.searchRepos(FAKE_SEARCH, FAKE_SORT)
        assertThat(errorData, `is`(instanceOf(ErrorResult::class.java)))
        assertThat((errorData as ErrorResult).message, `is`(FAKE_MESSAGE))
        assertThat(errorData.throwable?.message, `is`(FAKE_MESSAGE))
    }

    @Test
    fun testSuccessSearchDataEmptyList() = runBlocking {
        given(repoDataSource.searchRepos(FAKE_SEARCH, FAKE_SORT)).willReturn(Success(emptyList()))
        val emptyData = repoDataSource.searchRepos(FAKE_SEARCH, FAKE_SORT)
        assertThat(emptyData, `is`(instanceOf(Success::class.java)))
        assertThat((emptyData as Success).data.size, `is`(0))
    }

    @Test
    fun testSeccessSearchDataList() = runBlocking {
        given(repoDataSource.searchRepos(FAKE_SEARCH, FAKE_SORT)).willReturn(Success(getRepoData()))
        val data = repoDataSource.searchRepos(FAKE_SEARCH, FAKE_SORT)
        assertThat(data, `is`(instanceOf(Success::class.java)))
        assertThat((data as Success).data.size, `is`(3))

    }

    private fun getRepoData() = mutableListOf<RepositoryEntity>().apply {
        add(createFakeRepo(100))
        add(createFakeRepo(200))
        add(createFakeRepo(300))
    }

    private fun createFakeRepo(id: Long): RepositoryEntity {
        return RepositoryEntity(
            id = id,
            name = "github",
            description = "site of github",
            forks = 5,
            full_name = "Github sezam",
            open_issues_count = 9,
            watchers = 3,
            owner = createFakeUserEntity()
        )
    }

    private fun createFakeUserEntity(): UserEntity {
        return UserEntity(
            id = 200,
            avatar_url = "avatar_url",
            login = "makurt"
        )
    }

}