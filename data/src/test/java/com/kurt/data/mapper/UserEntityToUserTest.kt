package com.kurt.data.mapper

import com.kurt.data.entity.UserEntity
import com.kurt.domain.User
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.instanceOf
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class UserEntityToUserTest {

    private val FAKE_ID = 100L

    private lateinit var userMapper: Mapper<UserEntity, User>

    @Before
    fun setUp() {
        userMapper = UserEntiityToUser()
    }

    @Test
    fun transformUserEntityToUser() = runBlocking {
        val userEntity = createFakeUser()
        val user = userMapper.map(userEntity)
        assertThat(user, `is`(instanceOf(User::class.java)))
        assertThat(user.id, `is`(FAKE_ID))
    }

    private fun createFakeUser(): UserEntity {
        return UserEntity(
            id = FAKE_ID,
            login = "makurt",
            avatar_url = "html"
        )
    }

}