package com.kurt.data.mapper

import com.kurt.data.entity.RepositoryEntity
import com.kurt.data.entity.UserEntity
import com.kurt.domain.Repo
import com.kurt.domain.User
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class RepositoryEntityToRepoTest {

    private val FAKE_REPO_ID = 100L

    lateinit var userMapper: UserEntiityToUser

    private lateinit var repoMapper: Mapper<RepositoryEntity, Repo>

    @Before
    fun setUp() {
        userMapper = UserEntiityToUser()
        repoMapper = RepositoryEntityToRepo(userMapper)
    }

    @Test
    fun transformRepoEntityToRepo() = runBlocking {
        val repoEntity = createFakeRepoEntity()
        val repo = repoMapper.map(repoEntity)

        assertThat(repo, `is`(CoreMatchers.instanceOf(Repo::class.java)))
        assertThat(repo.id, `is`(FAKE_REPO_ID))

    }

    private fun createFakeRepoEntity(): RepositoryEntity {
        return RepositoryEntity(
            id = FAKE_REPO_ID,
            name = "github",
            description = "site of github",
            forks = 5,
            full_name = "Github sezam",
            open_issues_count = 9,
            watchers = 3,
            owner = createFakeUserEntity()
        )
    }

    private fun createFakeUserEntity(): UserEntity {
        return UserEntity(
            id = 200,
            avatar_url = "avatar_url",
            login = "makurt"
        )
    }

}