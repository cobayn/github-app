package com.kurt.data.mapper

import com.kurt.data.entity.UserDetailsEntiity
import com.kurt.domain.UserDetails
import javax.inject.Inject

class UserDetailsEntityToUserDetails @Inject constructor() : Mapper<UserDetailsEntiity, UserDetails> {

    override suspend fun map(from: UserDetailsEntiity): UserDetails {
        return UserDetails(
            id = from.id,
            html_url = from.html_url,
            login = from.login,
            avatar_url = from.avatar_url,
            name = from.name,
            location = from.location,
            company = from.company,
            email = from.email,
            bio = from.bio
        )
    }

}