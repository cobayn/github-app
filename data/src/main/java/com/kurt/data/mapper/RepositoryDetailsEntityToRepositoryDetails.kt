package com.kurt.data.mapper

import com.kurt.data.entity.RepositoryDetaiilsEntiity
import com.kurt.domain.RepoDetails
import javax.inject.Inject

class RepositoryDetailsEntityToRepositoryDetails @Inject constructor(
    private val userMapper: UserEntiityToUser
) : Mapper<RepositoryDetaiilsEntiity, RepoDetails> {

    override suspend fun map(from: RepositoryDetaiilsEntiity): RepoDetails {
        return RepoDetails(
            id = from.id,
            name = from.name,
            owner = userMapper.map(from.owner),
            watchers = from.watchers,
            open_issues_count = from.open_issues_count,
            full_name = from.full_name,
            forks = from.forks,
            description = from.description,
            html_url = from.html_url,
            language = from.language,
            created_at = from.created_at,
            updated_at = from.updated_at
        )
    }

}