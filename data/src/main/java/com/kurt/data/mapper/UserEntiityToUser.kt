package com.kurt.data.mapper

import com.kurt.data.entity.UserEntity
import com.kurt.domain.User
import javax.inject.Inject

class UserEntiityToUser @Inject constructor() : Mapper<UserEntity, User> {

    override suspend fun map(from: UserEntity): User {
        return User(
            id = from.id,
            login = from.login,
            avatar_url = from.avatar_url
        )
    }

}