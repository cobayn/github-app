package com.kurt.data.mapper

import com.kurt.data.entity.RepositoryEntity
import com.kurt.data.entity.RepositoryResponse
import javax.inject.Inject

class RepositoryResponseToRepositoryEntity @Inject constructor() : Mapper<RepositoryResponse, List<RepositoryEntity>> {

    override suspend fun map(from: RepositoryResponse): List<RepositoryEntity> {
        return from.items
    }

}