package com.kurt.data.mapper

import com.kurt.data.entity.RepositoryEntity
import com.kurt.domain.Repo
import javax.inject.Inject

class RepositoryEntityToRepo @Inject constructor(
    private val userMapper: UserEntiityToUser
) : Mapper<RepositoryEntity, Repo> {

    override suspend fun map(from: RepositoryEntity): Repo {
        return Repo(
            id = from.id,
            name = from.name,
            description = from.description,
            forks = from.forks,
            full_name = from.full_name,
            open_issues_count = from.open_issues_count,
            watchers = from.watchers,
            owner = userMapper.map(from.owner)
        )
    }

}