package com.kurt.data.entity

data class RepositoryDetaiilsEntiity(
    val id: Long,
    val name: String,
    val full_name: String,
    val html_url: String,
    val description: String,
    val owner: UserEntity,
    val forks: Int,
    val watchers: Int,
    val open_issues_count: Int,
    val language: String?,
    val created_at: String,
    val updated_at: String
)