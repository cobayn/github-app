package com.kurt.data.entity

data class UserEntity(
    val id: Long,
    val login: String,
    val avatar_url: String
    /*val name: String,
    val company: String,
    val location: String,
    val email: String?,
    val followers: Long,
    val following: Long*/
)