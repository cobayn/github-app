package com.kurt.data.entity

data class UserDetailsEntiity(
    val id: Long,
    val login: String,
    val avatar_url: String,
    val html_url: String,
    val name: String,
    val company: String,
    val location: String,
    val email: String?,
    val bio: String?
)