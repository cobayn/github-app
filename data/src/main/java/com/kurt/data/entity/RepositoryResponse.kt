package com.kurt.data.entity

data class RepositoryResponse(
    val total_count: Int,
    val items: List<RepositoryEntity>
)