package com.kurt.data.entity

data class RepositoryEntity(
    val id: Long,
    val name: String,
    val full_name: String,
    val description: String?,
    val forks: Int,
    val watchers: Int,
    val open_issues_count: Int,
    val owner: UserEntity
)