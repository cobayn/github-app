package com.kurt.data.entity

sealed class Result<T> {
    open fun get(): T? = null
}

data class Success<T>(val data: T) : Result<T>() {
    override fun get(): T = data
}

data class ErrorResult<T>(
    val throwable: Throwable? = null,
    val message: String? = null
) : Result<T>()