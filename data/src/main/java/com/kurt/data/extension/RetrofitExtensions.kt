package com.kurt.data.extension

import com.kurt.data.entity.ErrorResult
import com.kurt.data.entity.Result
import com.kurt.data.entity.Success
import com.kurt.data.mapper.Mapper
import retrofit2.HttpException
import retrofit2.Response

fun <T> Response<T>.bodyOrThrow(): T {
    if (!isSuccessful) throw HttpException(this)
    return body()!!
}

fun <T> Response<T>.toException() = HttpException(this)

suspend fun <T, E> Response<T>.toResult(mapper: suspend (T) -> E): Result<E> {

    return try {

        if (isSuccessful) {

            Success(data = mapper(bodyOrThrow()))

        } else {
            ErrorResult(toException())
        }

    } catch (e: Exception) {
        ErrorResult(e)
    }

}

fun <F, T> Mapper<F, T>.toListMapper(): suspend (F) -> T {
    return {list -> map(list)}
}