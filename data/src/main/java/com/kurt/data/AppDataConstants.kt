package com.kurt.data

object AppDataConstants {

    const val BASE_URL = "https://api.github.com/"
    const val SEARCH_REPO_URL = "search/repositories"
    const val REPO_DETAILS_URL = "repos/{owner}/{repo}"
    const val USER_DETAILS_URL = "users/{user}"

}