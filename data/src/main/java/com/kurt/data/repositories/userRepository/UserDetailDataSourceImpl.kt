package com.kurt.data.repositories.userRepository

import com.kurt.data.entity.Result
import com.kurt.data.entity.UserDetailsEntiity
import com.kurt.data.extension.toResult
import com.kurt.data.net.UserApi
import javax.inject.Inject

class UserDetailDataSourceImpl @Inject constructor(
    private val userApi: UserApi
): UserDetailDataSource {

    override suspend fun getUserDetails(username: String): Result<UserDetailsEntiity> {
        return userApi.getUserDetails(username).toResult { it }
    }

}