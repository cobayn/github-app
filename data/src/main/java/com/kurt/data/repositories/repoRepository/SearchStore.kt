package com.kurt.data.repositories.repoRepository

import android.util.LruCache
import com.kurt.data.entity.RepositoryEntity
import javax.inject.Inject

class SearchStore @Inject constructor() {

    private val cache = LruCache<Pair<String, String>, List<RepositoryEntity>>(32)

    fun getResults(querySort: Pair<String, String>) = cache[querySort]

    fun setResults(querySort: Pair<String, String>, results: List<RepositoryEntity>) {
        cache.put(querySort, results)
    }

}