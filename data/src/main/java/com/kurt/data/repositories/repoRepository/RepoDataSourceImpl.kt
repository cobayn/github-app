package com.kurt.data.repositories.repoRepository

import com.kurt.data.entity.RepositoryEntity
import com.kurt.data.entity.Result
import com.kurt.data.extension.toListMapper
import com.kurt.data.extension.toResult
import com.kurt.data.mapper.RepositoryResponseToRepositoryEntity
import com.kurt.data.net.RepositoryApi
import javax.inject.Inject

class RepoDataSourceImpl @Inject constructor(
    private val repositoryApi: RepositoryApi,
    private val repositoryResponseMapper: RepositoryResponseToRepositoryEntity
) : RepoDataSource {

    override suspend fun searchRepos(query: String, sort: String): Result<List<RepositoryEntity>> {
        return repositoryApi.searchRepositories(getQueryMap(query, sort)).toResult(repositoryResponseMapper.toListMapper())
    }

    private fun getQueryMap(query: String, sort: String) = mutableMapOf<String, String>().apply {
        put("q", query)
        put("sort", sort)
    }

}