package com.kurt.data.repositories.repoRepository

import com.kurt.data.entity.ErrorResult
import com.kurt.data.entity.Success
import com.kurt.data.mapper.RepositoryEntityToRepo
import com.kurt.domain.Repo
import com.kurt.domain.exception.NoInternetException
import com.kurt.domain.reposiitory.SearchRepository
import javax.inject.Inject

class RepoRepositoryImpl @Inject constructor(
    private val repositoryDataSource: RepoDataSource,
    private val repoMapper: RepositoryEntityToRepo,
    private val searchStore: SearchStore
) : SearchRepository {

    override suspend fun searchRepo(query: String, sort: String) : List<Repo> {

        if (query.isEmpty())
            return emptyList()

        val querySort = Pair(query, sort)
        val repoList = searchStore.getResults(querySort)
        if (repoList != null){
            return repoList.map {
                repoMapper.map(it)
            }
        }

        return when (val result = repositoryDataSource.searchRepos(query, sort)) {

            is Success -> {
                result.data
                    .also {
                        searchStore.setResults(querySort, it)
                    }
                    .map {
                    repoMapper.map(it)
                }
            }
            else -> emptyList()

        }

    }

}