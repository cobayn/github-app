package com.kurt.data.repositories.repoRepository

import com.kurt.data.entity.Result
import com.kurt.data.entity.RepositoryEntity

interface RepoDataSource {
    suspend fun searchRepos(query: String, sort: String) : Result<List<RepositoryEntity>>
}