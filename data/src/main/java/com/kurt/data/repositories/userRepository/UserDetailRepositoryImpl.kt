package com.kurt.data.repositories.userRepository

import com.kurt.data.entity.Success
import com.kurt.data.mapper.UserDetailsEntityToUserDetails
import com.kurt.data.mapper.UserEntiityToUser
import com.kurt.domain.User
import com.kurt.domain.UserDetails
import com.kurt.domain.exception.NoInternetException
import com.kurt.domain.exception.NoUserDetailException
import com.kurt.domain.reposiitory.UserDetailsRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class UserDetailRepositoryImpl @Inject constructor(
    private val userDetailDataSource: UserDetailDataSource,
    private val userMapper: UserDetailsEntityToUserDetails
): UserDetailsRepository{

    override suspend fun getUserDetails(username: String): UserDetails {
        return when (val response = userDetailDataSource.getUserDetails(username)){
            is Success ->
                userMapper.map(response.data)

            else ->
                throw NoUserDetailException()
        }
    }

}