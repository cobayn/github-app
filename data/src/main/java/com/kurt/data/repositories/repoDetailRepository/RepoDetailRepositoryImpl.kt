package com.kurt.data.repositories.repoDetailRepository

import com.kurt.data.entity.Success
import com.kurt.data.mapper.RepositoryDetailsEntityToRepositoryDetails
import com.kurt.data.mapper.RepositoryEntityToRepo
import com.kurt.domain.Repo
import com.kurt.domain.RepoDetails
import com.kurt.domain.exception.NoRepositoryDetailException
import com.kurt.domain.reposiitory.RepoDetailsRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import java.lang.RuntimeException
import javax.inject.Inject

class RepoDetailRepositoryImpl @Inject constructor(
    private val repoDetailDataSource: RepoDetailDataSource,
    private val repoMapper: RepositoryDetailsEntityToRepositoryDetails
): RepoDetailsRepository {

    override suspend fun getRepoDetails(owner: String, repo: String): RepoDetails {

        return when (val result = repoDetailDataSource.getRepoDetails(owner, repo)){

            is Success ->
                repoMapper.map(result.data)

            else ->
                throw NoRepositoryDetailException()

        }

    }

}