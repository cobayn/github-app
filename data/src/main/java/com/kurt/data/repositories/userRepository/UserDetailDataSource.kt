package com.kurt.data.repositories.userRepository

import com.kurt.data.entity.Result
import com.kurt.data.entity.UserDetailsEntiity

interface UserDetailDataSource {
    suspend fun getUserDetails(username: String): Result<UserDetailsEntiity>
}