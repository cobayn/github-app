package com.kurt.data.repositories.repoDetailRepository

import com.kurt.data.entity.RepositoryDetaiilsEntiity
import com.kurt.data.entity.Result

interface RepoDetailDataSource {
    suspend fun getRepoDetails(owner: String, repo: String): Result<RepositoryDetaiilsEntiity>
}