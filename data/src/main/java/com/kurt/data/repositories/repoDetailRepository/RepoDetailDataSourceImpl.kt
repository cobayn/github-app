package com.kurt.data.repositories.repoDetailRepository

import com.kurt.data.entity.RepositoryDetaiilsEntiity
import com.kurt.data.entity.Result
import com.kurt.data.extension.toResult
import com.kurt.data.net.RepositoryApi
import javax.inject.Inject

class RepoDetailDataSourceImpl @Inject constructor(
    private val repositoryApi: RepositoryApi
): RepoDetailDataSource {

    override suspend fun getRepoDetails(owner: String, repo: String): Result<RepositoryDetaiilsEntiity> {
        return repositoryApi.getRepositoryDetails(owner, repo).toResult { it }
    }

}