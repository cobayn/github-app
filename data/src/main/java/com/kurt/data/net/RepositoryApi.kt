package com.kurt.data.net

import com.kurt.data.AppDataConstants.REPO_DETAILS_URL
import com.kurt.data.AppDataConstants.SEARCH_REPO_URL
import com.kurt.data.entity.RepositoryDetaiilsEntiity
import com.kurt.data.entity.RepositoryResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.QueryMap

interface RepositoryApi {

    @GET(SEARCH_REPO_URL)
    suspend fun searchRepositories(@QueryMap queries: Map<String, String>): Response<RepositoryResponse>

    @GET(REPO_DETAILS_URL)
    suspend fun getRepositoryDetails(@Path("owner") owner: String, @Path("repo") repo: String): Response<RepositoryDetaiilsEntiity>

}