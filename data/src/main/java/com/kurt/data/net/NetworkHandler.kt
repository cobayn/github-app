package com.kurt.data.net

import android.content.Context
import android.net.ConnectivityManager
import javax.inject.Inject

object NetworkHandler {

    fun isThereInternetConnection(context: Context): Boolean {
        val cm = context.applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        val activeNetwork = cm.activeNetworkInfo
        return activeNetwork != null && activeNetwork.isConnected
    }

}