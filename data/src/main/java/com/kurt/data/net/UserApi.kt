package com.kurt.data.net

import com.kurt.data.AppDataConstants.USER_DETAILS_URL
import com.kurt.data.entity.UserDetailsEntiity
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface UserApi {

    @GET(USER_DETAILS_URL)
    suspend fun getUserDetails(@Path("user") user: String): Response<UserDetailsEntiity>

}