package com.kurt.domain

data class Repo(
    override val id: Long,
    override val name: String,
    override val full_name: String,
    override val description: String?,
    override val forks: Int,
    override val watchers: Int,
    override val open_issues_count: Int,
    override val owner: User
): BaseRepo