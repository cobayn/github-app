package com.kurt.domain

data class User(
    override val id: Long,
    override val login: String,
    override val avatar_url: String
    /*val name: String,
    val company: String,
    val location: String,
    val email: String?,
    val followers: Long,
    val following: Long*/
): BaseUser