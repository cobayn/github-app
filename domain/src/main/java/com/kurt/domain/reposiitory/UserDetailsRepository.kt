package com.kurt.domain.reposiitory

import com.kurt.domain.UserDetails

interface UserDetailsRepository {

    suspend fun getUserDetails(username: String): UserDetails

}