package com.kurt.domain.reposiitory

import com.kurt.domain.RepoDetails

interface RepoDetailsRepository {

    suspend fun getRepoDetails(owner: String, repo: String): RepoDetails

}