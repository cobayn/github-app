package com.kurt.domain.reposiitory

import com.kurt.domain.Repo

interface SearchRepository {
    suspend fun searchRepo(query: String, sort: String): List<Repo>
}