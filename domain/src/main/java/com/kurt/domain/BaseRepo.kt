package com.kurt.domain

interface BaseRepo {
    val id: Long
    val name: String
    val full_name: String
    val description: String?
    val forks: Int
    val watchers: Int
    val open_issues_count: Int
    val owner: BaseUser
}