package com.kurt.domain

data class RepoDetails(
    override val id: Long,
    override val name: String,
    override val full_name: String,
    override val description: String?,
    override val forks: Int,
    override val watchers: Int,
    override val open_issues_count: Int,
    override val owner: User,
    override val html_url: String,
    val language: String?,
    val created_at: String,
    val updated_at: String
): DetailModel, BaseRepo