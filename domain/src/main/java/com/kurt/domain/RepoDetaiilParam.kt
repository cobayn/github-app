package com.kurt.domain

data class RepoDetaiilParam(
    val repo: String,
    override val owner: String
) : DetailParam