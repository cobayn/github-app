package com.kurt.domain

data class UserDetails(
    override val id: Long,
    override val login: String,
    override val avatar_url: String,
    override val html_url: String,
    val name: String,
    val company: String?,
    val location: String?,
    val email: String?,
    val bio: String?
): DetailModel, BaseUser