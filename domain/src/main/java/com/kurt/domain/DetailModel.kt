package com.kurt.domain

interface DetailModel {
    val html_url: String
}