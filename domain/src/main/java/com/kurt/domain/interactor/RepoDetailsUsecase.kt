package com.kurt.domain.interactor

import com.kurt.domain.RepoDetaiilParam
import com.kurt.domain.RepoDetails
import com.kurt.domain.reposiitory.RepoDetailsRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

class RepoDetailsUsecase @Inject constructor(
    private val repoDetailsRepository: RepoDetailsRepository
) : ResultInteractor<RepoDetaiilParam, RepoDetails>(){

    override val dispatcher: CoroutineDispatcher
        get() = Dispatchers.Main

    override suspend fun doWork(params: RepoDetaiilParam): RepoDetails {
        return repoDetailsRepository.getRepoDetails(params.owner, params.repo)
    }

}