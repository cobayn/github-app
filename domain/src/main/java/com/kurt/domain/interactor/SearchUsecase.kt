package com.kurt.domain.interactor

import com.kurt.domain.Repo
import com.kurt.domain.reposiitory.SearchRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

class SearchUsecase @Inject constructor(
    private val searchRepository: SearchRepository
) : SuspendingInteractor<SearchUsecase.Params, List<Repo>>() {

    override val dispatcher: CoroutineDispatcher
        get() = Dispatchers.Main

    override suspend fun doWork(params: Params): List<Repo> {
        return searchRepository.searchRepo(params.query, params.sort)
    }

    data class Params(val query: String, val sort: String)

}