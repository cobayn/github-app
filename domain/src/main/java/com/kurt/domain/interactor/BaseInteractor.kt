package com.kurt.domain.interactor

import kotlinx.coroutines.*
import kotlinx.coroutines.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.asFlow
import java.util.concurrent.TimeUnit

interface ObservableInteractor<T> {
    val dispatcher: CoroutineDispatcher
    fun observe(): Flow<T>
}

abstract class ResultInteractor<in P, R> {
    abstract val dispatcher: CoroutineDispatcher

    suspend operator fun invoke(params: P): R {
        return withContext(dispatcher) { doWork(params) }
    }

    protected abstract suspend fun doWork(params: P): R
}

abstract class SuspendingInteractor<P : Any, T : Any> : ObservableInteractor<T>{

    private val channel = ConflatedBroadcastChannel<T>()

    suspend operator fun invoke(params: P) = channel.send(doWork(params))

    abstract suspend fun doWork(params: P): T

    override fun observe(): Flow<T> = channel.asFlow()

}

fun <I : ObservableInteractor<T>, T> CoroutineScope.launchObserve(interactor: I, exception: CoroutineExceptionHandler, f: suspend (Flow<T>) -> Unit) {
    launch(interactor.dispatcher + exception) {
        f(interactor.observe())
    }
}
