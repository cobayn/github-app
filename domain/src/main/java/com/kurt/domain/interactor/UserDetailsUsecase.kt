package com.kurt.domain.interactor

import com.kurt.domain.UserDetailParam
import com.kurt.domain.UserDetails
import com.kurt.domain.reposiitory.UserDetailsRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

class UserDetailsUsecase @Inject constructor(
    private val userDetailsRepository: UserDetailsRepository
): ResultInteractor<UserDetailParam, UserDetails>() {

    override val dispatcher: CoroutineDispatcher
        get() = Dispatchers.Main

    override suspend fun doWork(params: UserDetailParam): UserDetails {
        return userDetailsRepository.getUserDetails(params.owner)
    }

}