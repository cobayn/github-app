package com.kurt.domain

interface BaseUser {
    val id: Long
    val login: String
    val avatar_url: String
}